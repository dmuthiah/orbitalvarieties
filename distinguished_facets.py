from read_rings_from_file import pre_skew_tableau_from_list_of_positions
from sage.all import SkewTableau, StandardTableaux

#Uncomment below to compute facets
#load('matrix_of_springer_representation.py')
#positions_dict = compute_positions_up_to_7()

#Takes about a minute to run. This is a dict of the blank slots in the facets.

def compute_distinguished_facets(positions_dict):
    dist_facets_dict = {}
    for tableau in positions_dict.keys():
        n = tableau.size()
        dist_facet = find_distinguished_facet(tableau,positions_dict[tableau])
        facet_as_skew_tableau = skew_tableau_from_list_of_positions_complement(dist_facet,n)
        dist_facets_dict[tableau] = facet_as_skew_tableau
    return dist_facets_dict

def find_distinguished_facet(tableau,facet_list):
    """
    This is wrong currently.
    """
    first_row = tableau[0]
    for facet in facet_list:
        non_dist = False
        for position in facet:
            if non_dist == True:
                break
            for i in first_row:
                if position[1] == i:
                    non_dist = True
        if non_dist==False:
            return facet

def skew_tableau_from_list_of_positions_complement(positions,n):
    r"""Given a list of tuples (i,j) with 1 <=  i < j <= n
    return the skew tableaus
    """
    array = [[None for a in xrange(n)] for b in xrange(n)] #mutable square array
    for i in xrange(1,n+1):
        for j in xrange(i+1,n+1):
            if (i,j) in positions:
                array[i-1][j-1] = ""
            else:
                array[i-1][j-1] = "*"
    return SkewTableau(array)

def make_tables_of_distinguised_facets_up_to(dist_facets_dict,N):
    tables = []
    for n in xrange(2,N+1):
        for T in StandardTableaux(n):
            L = (T,dist_facets_dict[T])
            tables.append(table(L))
    return tables
