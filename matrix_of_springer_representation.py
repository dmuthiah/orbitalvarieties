""" 2018-06-21 - In this file, we will compute the matrix of simple reflections on Springer reps.
"""
from read_rings_from_file import compute_up_to_7_orbital_varieties
from sage.all import StandardTableaux,Partition


def compute_positions_up_to_7():

    R,I = compute_up_to_7_orbital_varieties()
    positions_dict = {}
    for n in xrange(2,8):
        for partition in Partitions(n):
            for T in I:
                if T.shape() != partition:
                    continue
                #L = I[T].gens()
                L = I[T].groebner_basis()
                leading_terms = [term.lt() for term in L]
                initial_ideal = ideal(leading_terms)
                facet_primes = initial_ideal.minimal_associated_primes()
                positions_dict[T] = []
                for p in facet_primes:
                    positions = []
                    pgens = p.gens()
                    for i in xrange(1,n+1):
                        for j in xrange(i+1,n+1):
                            if R[n]("Delta%d%d"%(i,j)) in pgens:
                               positions.append((i,j))
                    positions_dict[T].append(positions)
    return positions_dict


class JosephPolyCalculator(object):

    def __init__(self,positions_dict):
        self.positions_dict = positions_dict
        self._create_rings()

    def _create_rings(self):
        self.R = {}
        for i in xrange(2,8):
            var_names = ["x_%d" % j for j in xrange(1,i+1)]
            self.R[i] = PolynomialRing(QQ,var_names)
    
    def joseph_poly(self,tableau):
        n = tableau.size()
        X = self.R[n].gens()
        output = self.R[n].zero()
        for position_list in self.positions_dict[tableau]:
            term = self.R[n].one()
            for (i,j) in position_list:
                term *= X[j-1] - X[i-1]
            output += term
        return output

    def act_by_simple_reflection(self,i,poly):
        R = poly.parent()
        X = R.gens()
        coeff_dict = poly.dict()
        new_dict = {}
        for key in coeff_dict:
            mutable_key = list(key)
            temp = mutable_key[i-1]
            mutable_key[i-1] = mutable_key[i]
            mutable_key[i] = temp
            new_dict[tuple(mutable_key)] = coeff_dict[key]
        return R(new_dict)
    
        
    def expand_poly_as_column_vector(self,poly):
        R = poly.parent()
        X = R.gens()
        n = len(X)
        assert(poly.is_homogeneous())
        d = poly.total_degree() 
        basis = [monomial for monomial in monomials(X,[d+1]*n) if monomial.total_degree()==d]
        vector = [poly.coefficient(monomial) for monomial in basis]
        return matrix(vector).transpose()


    def expand_in_joseph_poly_basis(self,poly,shape):
        tableaux = list(StandardTableaux(shape))
        vectors = []
        for tableau in tableaux:
           vectors.append(tuple(self.expand_poly_as_column_vector(self.joseph_poly(tableau)).transpose())[0])
        A = matrix(vectors,ring=RR).transpose()
        b = self.expand_poly_as_column_vector(poly).apply_map(RR)
        v_real = A.pseudoinverse()*b
        v = v_real.apply_map(lambda x: x.round())
        assert((v-v_real).norm()<RR(1e-10))
        return v

    def matrix_of_simple_reflection(self,i,shape):
        tableaux = list(StandardTableaux(shape))
        joseph_polys = [self.joseph_poly(tableau) for tableau in tableaux]
        reflected_joseph_polys = [calc.act_by_simple_reflection(i,f) for f in joseph_polys]
        #print joseph_polys
        #print reflected_joseph_polys
        A = matrix(
            [list(calc.expand_poly_as_column_vector(f).transpose())[0] for f in joseph_polys],ring=RR).transpose()
        #print A
        #print "-----"
        B = matrix(
            [list(calc.expand_poly_as_column_vector(f).transpose())[0] for f in reflected_joseph_polys],ring=RR).transpose()
        #print B
        M_real = A.pseudoinverse()*B
        #print M_real
        M = M_real.apply_map(lambda x: x.round())
        assert((M-M_real).norm()<RR(1e-10))
        return M

        
