from sage.all import SR, matrix, floor, det, Macaulay2, expand, PolynomialRing, QQ
from string import join
import re



class SL_n_Gr_Lambda_Ideal_Calculator(object):
    """
    Calculate the ideal defined in [KWWY] that gives the modular 
    definition of Gr^\lambda. Use notation that will be usable in Macaulay2.
    """
    

    def __init__(self,n):
        self.n = n

    def matrix_entry(self,row_number,column_number):
        
        n = self.n  #Transplanted function to method

        if n==2:
            pass
            #return matrix_entry_sl2(row_number,column_number)
        elif n==0:
            raise NotImplementedError
            return matrix_entry_gl_infty(row_number,column_number)

        #n = __name__.n
        exponent = floor(column_number/n)-floor(row_number/n)
        finite_row_number = row_number % n + 1
        finite_col_number = column_number % n + 1

        if exponent < 0:
            return 0
        elif exponent == 0:
            if finite_row_number==finite_col_number:
                return 1
            else:
                return 0
        else:
            return SR.var("Delta_"+str(exponent)+"_"+str(finite_row_number)+"_"+str(finite_col_number),latex_name="{\Delta^{("+str(exponent)+")}_{"+str(finite_row_number)+","+str(finite_col_number)+"}}")

    def A(self,s):
        n = self.n
        return matrix([[self.matrix_entry(i,j) for j in xrange(n*s,n*(s+1))] for i in xrange(n)])
    

    def clean_expression_for_macaulay2(self,expression):
        def _replace_underscores_with_braces(matchobj):
            groups = matchobj.groups()
            return "{" + groups[0] + "," + groups[1] + "," + groups[2] + "}"
        return re.sub(r"(\d+)[_](\d+)[_](\d+)",_replace_underscores_with_braces,expression)
        
    def ideal(self,dominant_coweight,clean_for_macaulay2=True):
        """
        ``dominant_coweight`` should be a (`self.n`-1)-tuple which is the coefficients 
        when expanded in the basis of simple coroots. For now, we require ``dominant_coweight``
        to be an element of the coroot lattice.
        """
        t = SR.var('t')
        ideal_generators = []
        ring_generators = []
        for j,value in enumerate(reversed(dominant_coweight)):
            k = j+1
            cutoff = value + 1
            if k==1:
                g = sum(t**s * self.A(s) for s in xrange(cutoff))
                cutoff1 = cutoff
                entries = g.minors(1)
                for entry in entries:
                    ring_generators += [entry.coefficient(t,s) 
                                        for s in xrange(1,cutoff)]
            else:
                k_minors = g.minors(k)
                for minor in k_minors:
                    ideal_generators += [expand(minor).coefficient(t,s) 
                                         for s in xrange(cutoff,k*cutoff1+1)]
        ideal_generators += zip(*det(g).coefficients(t))[0][1:] #Hack to extract all the first elements of a list of tuples (not the first, which is 1
        if clean_for_macaulay2:
            ring_generators = list(sorted([self.clean_expression_for_macaulay2(str(gen))
                                for gen in ring_generators if gen != 0 ]))
            ideal_generators = [self.clean_expression_for_macaulay2(str(gen))
                                for gen in ideal_generators if gen != 0 ]

        return (ring_generators,ideal_generators)
        

    def ring_and_ideal_as_sage_object(self,dominant_coweight):
        """
        Return a pair ``(output_ring,output_ideal)`` where ``output_ring`` is a 
        Sage polynomial ring and ``output_ideal_gens`` is a list of 
        ideal generators.
        """
        (ring_gens_in_sym_ring,ideal_gens_in_sym_ring) = self.ideal(dominant_coweight,
                                                                    clean_for_macaulay2=False)
        var_names = map(str,ring_gens_in_sym_ring)
        output_ring = PolynomialRing(QQ,var_names)
        output_ideal_gens = map(output_ring,ideal_gens_in_sym_ring)
        return (output_ring,output_ideal_gens)


                            
    def compute_groebner_basis(self,dominant_coweight):
        ring_gens,ideal_gens = self.ideal(dominant_coweight)
        macaulay2 = Macaulay2()
        macaulay2.eval("R = QQ["+join(ring_gens,",")+"]")
        macaulay2.eval("I = ideal ("+join(ideal_gens,",")+")")
        macaulay2.eval("g = groebnerBasis I")
        return macaulay2,ring_gens,ideal_gens

