

from string import join
from sage.all import Tableau, PolynomialRing, QQ

def read_base_rings_into_mac2commands(filename):
    f = open(filename,"r")
    items = []
    for line in f:
        items.append(eval(line))
    items = [(n,eval(l)) for n,l in items]
    ring_dict = {k: "QQ["+join(ring_gens,",")+"]" for k,ring_gens in items}
    return ring_dict

def read_base_rings_into_lists_for_sage(filename):
    f = open(filename,"r")
    items = []
    for line in f:
        items.append(eval(line))
    items = [(n,eval(l)) for n,l in items]
    ring_dict = {k:v for k,v in items}
    return ring_dict

def read_orbital_varieties_into_mac2commands(filename):
    f = open(filename,"r")
    items = []
    for line in f:
        items.append(eval(line))
    ideal_dict = {Tableau(k):v for k,v in items} 
    return ideal_dict


def compute_up_to_7_orbital_varieties():
    try:
        base_rings = read_base_rings_into_lists_for_sage("base_rings_up_to_7_sage.txt")
        orb_vars = read_orbital_varieties_into_mac2commands("orbital_varieties_sage.txt")
        R = {}
        for n in xrange(2,8):
            R[n] = PolynomialRing(QQ,base_rings[n])
        I = {}
        for T in orb_vars:
            n = T.size()
            exec(str(R[n].gens())+"= R[n].gens()")
            exec("I[T] = R[n]."+orb_vars[T])
    except RuntimeError:
        print T
        print orb_vars[T]
        return T,orb_vars
    return (R,I)

"""
This doesn't work properly.
def compute_up_to_7_initial_ideals_of_orbital_varieties():
    try:
        base_rings = read_base_rings_into_lists_for_sage("base_rings_up_to_7_sage.txt")
        orb_vars = read_orbital_varieties_into_mac2commands("orbital_varieties_initial_ideals_sage.txt")
        R = {}
        for n in xrange(2,8):
            R[n] = PolynomialRing(QQ,base_rings[n])
        I = {}
        for T in orb_vars:
            n = T.size()
            exec(str(R[n].gens())+"= R[n].gens()")
            exec("I[T] = R[n]."+orb_vars[T])
    except RuntimeError:
        print T
        print orb_vars[T]
        return T,orb_vars
    return (R,I)
"""

def make_tables_of_facets_of_initial_ideals_up_to_7(R,I):
    tables = []
    for n in xrange(2,8):
        for partition in Partitions(n):
            for T in I:
                if T.shape() != partition:
                    continue
                min_primes = I[T].minimal_associated_primes()
                L = map(lambda x: x.gens(),min_primes)
                L = [[v] for v in L]
                L = [[T]]+L
                tables.append(table(L))
    return tables

def make_tables_of_orbital_varieties_up_to_7(R,I):
    tables = []
    for n in xrange(2,8):
        for partition in Partitions(n):
            for T in I:
                if T.shape() != partition:
                    continue
                L = I[T].gens()
                L = [[v] for v in L]
                L = [[T]]+L
                tables.append(table(L))
    return tables

def make_tables_of_orbital_varieties_up_to_7_compute_groebner(R,I):
    tables = []
    for n in xrange(2,8):
        for partition in Partitions(n):
            for T in I:
                if T.shape() != partition:
                    continue
                #L = I[T].gens()
                L = I[T].groebner_basis()
                L = [[v] for v in L]
                L = [[T]]+L
                tables.append(table(L))
    return tables

def make_tables_of_facets_up_to_7(R,I):
    tables = []
    for n in xrange(2,8):
        for partition in Partitions(n):
            for T in I:
                if T.shape() != partition:
                    continue
                #L = I[T].gens()
                L = I[T].groebner_basis()
                leading_terms = [term.lt() for term in L]
                initial_ideal = ideal(leading_terms)
                facet_primes = initial_ideal.minimal_associated_primes()
                facet_gens = [p.gens() for p in facet_primes]
                entries = [[v] for v in facet_gens]
                entries = [[T]]+entries
                tables.append(table(entries))
    return tables

def compute_facets_up_to_7(R,I):
    """
    Running the following seems reasonable:
    pre_tables = compute_facets_up_to_7(R,I)
    view(map(flatten,pre_tables),tightpage=True)
    """
    tables = []
    pre_tables = []
    for n in xrange(2,8):
        for partition in Partitions(n):
            for T in I:
                if T.shape() != partition:
                    continue
                #L = I[T].gens()
                L = I[T].groebner_basis()
                leading_terms = [term.lt() for term in L]
                initial_ideal = ideal(leading_terms)
                facet_primes = initial_ideal.minimal_associated_primes()
                skew_tableaux = []
                for p in facet_primes:
                    positions = []
                    pgens = p.gens()
                    for i in xrange(1,n+1):
                        for j in xrange(i+1,n+1):
                            if R[n]("Delta%d%d"%(i,j)) not in pgens:
                               positions.append((i,j))
                    skew_tableaux.append(
                         [SkewTableau(pre_skew_tableau_from_list_of_positions(positions,n))])
                facet_gens = [p.gens() for p in facet_primes]
                entries = [[T]]+skew_tableaux
                pre_tables.append(entries)
                tables.append(table(entries))
    return pre_tables

"""
#This is wrong because we are not computing Groebner bases 
def make_dict_of_facets_of_initial_ideals_up_to_7_collect_by_partition(R,I):
    facet_dict = {}
    for n in xrange(2,8):
        for partition in Partitions(n):
            facet_dict[partition] = []
            for T in I:
                if T.shape() != partition:
                    continue
                min_primes = I[T].minimal_associated_primes()
                L = map(lambda x: x.gens(),min_primes)
                facet_dict[partition].append(L)
    return facet_dict

def make_tables_of_facets_of_initial_ideals_up_to_7_collect_by_partition(facet_dict):
    tables = []
    for n in xrange(2,8):
        for partition in Partitions(n):
            L = facet_dict[partition]
            flat_list = [item for sublist in L for item in sublist]
            flat_list = sorted(flat_list)
            L = flat_list
            try:
                assert(len(L)==len(set(L)))
            except:
                print partition
                print L
            
            L = [[v] for v in L]
            L = [[partition]]+L
            tables.append(table(L))
    return tables
"""

def pre_skew_tableau_from_list_of_positions(positions,n):
    r"""Given a list of tuples (i,j) with 1 <=  i < j <= n
    return the skew tableae
    """
    array = [[None for a in xrange(n)] for b in xrange(n)] #mutable square array
    for i in xrange(1,n+1):
        for j in xrange(i+1,n+1):
            if (i,j) in positions:
                array[i-1][j-1] = "*"
            else:
                array[i-1][j-1] = ""
    return array
                
        
