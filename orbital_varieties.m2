pierizero = method()
pierizero(List, List, PolynomialRing) := (mu, boxes, P) -> (
     R := coefficientRing(P);
     d := numgens(P);
     result := pieriHelper(mu, boxes#0, P);
     for b from 1 to #boxes-1 do (
	  mu = subtractOne(mu, boxes#(b-1));
     	  result = pieriHelper(mu, boxes#b, P) * result;
	  );
     return result;
     )

coordRings = method()
coordRings(ZZ) := (n) -> (
-- Currently this pollutes the crap out of the namespace
    varNamesForNilp = flatten for i from 1 to n list ( for j from 1 to n list (if i < j then X_(i,j) else continue));
    R = QQ[varNamesForNilp];
    varNamesForUnip = flatten for i from 1 to n list ( for j from 1 to n list (if i < j then g_(i,j) else continue));
    S = QQ[varNamesForUnip];
    T = QQ[join(varNamesForUnip,varNamesForNilp)];
    M = matrix(for i from 1 to n list ( for j from 1 to n list (if i < j then X_(i,j) else 0)));
    G = matrix(for i from 1 to n list ( for j from 1 to n list (if i < j then g_(i,j) else if i==j then 1 else 0)));
    return (R,M,S,G); 
    )


coordRingOfNilpotent = method()
coordRingOfNilpotent(ZZ) := (n) -> (
    unflattenedVars := for i from 1 to n list ( for j from 1 to n list (if i < j then X_(i,j) else continue));
    varNames := flatten unflattenedVars;
    R := QQ[varNames];
    T := R[g];
    M = matrix(for i from 1 to n list ( for j from 1 to n list (if i < j then X_(i,j) else 0)));
    return (R,T,M,n); 
    )
    
coordRingOfUnipotent = method()
coordRingOfUnipotent(ZZ) := (n) -> (
    unflattenedVars := for i from 1 to n list ( for j from 1 to n list (if i < j then g_(i,j) else continue));
    varNames := flatten unflattenedVars;
    S := QQ[varNames];
    G = matrix(for i from 1 to n list ( for j from 1 to n list (if i < j then g_(i,j) else 0))); --Incorrect
    return (S,G); 
    )
   
   

 
applyChevSubgp = method();
applyChevSubgp(ZZ,Ideal,Sequence) := (k,I,paramSeq) -> (
    (R,T,M,n) := paramSeq;
    J = substitute(I,T);
    G = matrix(for i from 1 to n list ( for j from 1 to n list (if (i==k and j==k+1) then g else if i==j then 1 else 0)));
    N = G*M*inverse(G); 
    phi = map(T,R,flatten (for i from 1 to n list ( for j from 1 to n list (if i < j then N_(i-1,j-1) else continue))));
    return preimage(phi,J);
    ) 


computeInitialIdeal = method();
computeInitialIdeal(Ideal) := (I) -> (
    return monomialIdeal(leadTerm(I));
    )

-- var must be of the form X_(i,j)
variableToIndexList =  var -> new List from value(substring(2,toString var)) ;
    
-- You need to check that the input is a square-free-initial-ideal
computeSRFacets = method();
computeSRFacets(MonomialIdeal) := (I) -> (
    return apply(apply(apply(apply(
		    minimalPrimes(I),
		    gens),entries),L->L_0),L -> apply(L,variableToIndexList));
        
    )
     

