from sage.all import RootSystem, vector, Macaulay2, Partition, Partitions
from sage.combinat.tableau import from_chain
from string import join

from orbitalvarieties.sl_n_gr_lambda_ideal import SL_n_Gr_Lambda_Ideal_Calculator


def convert_partition_to_dominant_sl_n_coweight(partition):
    """
    INPUT: `partition` a partition of size n.
    OUTPUT: An (n-1)-tuple which is the dominant coweight corresponding to 
            `partition`-transpose
    """
    partition_transpose = partition.conjugate()
    n = partition.size()
    if len(partition_transpose)==1:
        return tuple([0 for i in xrange(n-1)])
    dom_coweight_expanded_in_fund_coweights = vector(partition_transpose.to_exp(n-1))
    dom_coweight_in_simple_coroot_basis = (RootSystem(["A",n-1]).cartan_matrix().inverse() *
                                       dom_coweight_expanded_in_fund_coweights)
    return dom_coweight_in_simple_coroot_basis


def partition_as_number_string(partition):
    return join(map(str,list(partition)),"")




def read_file_to_mac2(mac2,input_file):
    with open(input_file,"r") as f:
        lines = f.readlines()
        for command in lines:
            mac2(command[:-2]) #Strip newline
    return mac2

# class Orbital_Variety_Database(object):

#     def __init__(self):
#         self.base_ring_dict = {}
#         self.o_lambda_dict = {}
#         self.orbital_variety_dict = {}
#         self.n_values_used = set([])
    
#     def read_orbital_varieties(self,orbital_variety_file):
#         with orbital_variety_file as f:
#             lines = f.readlines()
#         evaluated_lines = map(eval,lines)
#         for tableau_datum,m2_string in evaluated_lines:
#             st = StandardTableau(tableau_datum)
#             if st not in self.orbital_variety_dict:
#                 self.orbital_variety_dict[st] = m2_string
            




class SL_n_Orbital_Variety_Calculator(object):
    """
    Calculate the ideal of an orbital variety using the equations for Gr^\lambda
    defined in [KWWY].
    """
    

    def __init__(self,n):
        self.n = n
        self.ideal_calc = SL_n_Gr_Lambda_Ideal_Calculator(n)
        self.ring_gens,_ = self.ideal(Partition([n]))
        

    def ideal(self,partition,return_sage_object=False):
        n = self.n
        dominant_coweight = convert_partition_to_dominant_sl_n_coweight(partition)
        if return_sage_object: 
            ring,ideal_gens = self.ideal_calc.ring_and_ideal_as_sage_object(dominant_coweight)
            ring_gens = ring.gens()
        else:
            ring_gens,ideal_gens = self.ideal_calc.ideal(dominant_coweight)
        for i in xrange(1,n+1):
            for j in xrange(1,n+1):
                index = (i-1)*n + (j-1)
                if i >= j:
                    ideal_gens.append(ring_gens[index])
        if return_sage_object: 
            return (ring,ideal_gens)
        else: 
            return (ring_gens,ideal_gens)
    
    def compute_irreducible_components(self,partition):
        ring_gens,ideal_gens = self.ideal(partition)
        macaulay2 = Macaulay2()
        macaulay2.eval("R = QQ["+join(ring_gens,",")+"]")
        macaulay2.eval("I = ideal ("+join(ideal_gens,",")+")")
        macaulay2.eval("g = groebnerBasis I")
        macaulay2.eval("M = minimalPrimes I")
        return macaulay2


    def compute_irreducible_components_with_gradings(self,partition):
        ring_gens,ideal_gens = self.ideal(partition)
        new_ring_gens,new_ideal_gens = self.clean_up(ring_gens,ideal_gens)
        macaulay2 = Macaulay2()
        print "R = QQ["+join(new_ring_gens,",")+", Degrees=> "+self.compute_degrees()+"]" 
        macaulay2.eval("R = QQ["+join(new_ring_gens,",")+", Degrees=> "+self.compute_degrees()+"]")
        macaulay2.eval("I = ideal ("+join(new_ideal_gens,",")+")")
        macaulay2.eval("g = groebnerBasis I")
        macaulay2.eval("M = minimalPrimes I")
        return macaulay2


    def compute_irreducible_components_with_gradings_pass_mac2(self,partition,mac2):
        if len(partition.conjugate())==1:
            ring_gens,ideal_gens = self.ring_gens,self.ring_gens
        else:
            ring_gens,ideal_gens = self.ideal(partition)
        new_ring_gens,new_ideal_gens = self.clean_up(ring_gens,ideal_gens)
        mac2.eval( "R" + str(self.n) +
                        " = QQ["+join(new_ring_gens,",")+
                        ", Degrees=> "+self.compute_degrees()+"]")
        suffix = partition_as_number_string(partition)
        mac2.eval("I" + suffix +
                       " = ideal ("+join(new_ideal_gens,",")+")")
        mac2.eval("g" + suffix +
                       " = groebnerBasis I" + suffix)
        mac2.eval("M" + suffix +
                       " = minimalPrimes I" + suffix)
        return mac2

    def compute_irreducible_components_with_gradings_pass_mac2_append_to_file(self,partition,mac2,out_file):
        if len(partition.conjugate())==1:
            ring_gens,ideal_gens = self.ring_gens,self.ring_gens
        else:
            ring_gens,ideal_gens = self.ideal(partition)
        new_ring_gens,new_ideal_gens = self.clean_up(ring_gens,ideal_gens)
        mac2.eval( "R" + str(self.n) +
                        " = QQ["+join(new_ring_gens,",")+
                        ", Degrees=> "+self.compute_degrees()+"]")
        suffix = partition_as_number_string(partition)
        mac2.eval("I" + suffix +
                       " = ideal ("+join(new_ideal_gens,",")+")")
        mac2.eval("g" + suffix +
                       " = groebnerBasis I" + suffix)
        mac2.eval("M" + suffix +
                       " = minimalPrimes I" + suffix)
        with open(out_file,"a") as f:
            f.write("R"+str(self.n)+" = " +mac2("toExternalString R"+str(self.n)).to_sage()+";\n")
            f.write("I" + suffix + " = " +mac2("toExternalString I" + suffix).to_sage()+";\n")
            f.write("g" + suffix + " = " +mac2("toExternalString g" + suffix).to_sage()+";\n")
            f.write("M" + suffix + " = " +mac2("toExternalString M" + suffix).to_sage()+";\n")
        return mac2


    def clean_up(self,ring_gens,ideal_gens):
        n = self.n
        new_ring_gens = []
        new_ideal_gens = ideal_gens
        for i in xrange(1,n+1):
            for j in xrange(1,n+1):
                index = (i-1)*n + (j-1)
                if i >= j:
                    var_string = "Delta_{1,%d,%d}" %(i,j)
                    new_ideal_gens = [gen.replace(var_string,"0") for gen in new_ideal_gens]
                else:
                    new_ring_gens.append(ring_gens[index])
        return new_ring_gens,new_ideal_gens

    def compute_degrees(self):
        n = self.n
        degrees_list = []
        for i in xrange(1,n+1):
            for j in xrange(1,n+1):
                index = (i-1)*n + (j-1)
                if i < j:
                    single_degree_list = [int(i<=k and k<j) for k in xrange(1,n)]
                    single_degree_to_string = str(single_degree_list).replace("[","{").replace("]","}")
                    degrees_list.append(single_degree_to_string)
        degrees_string = "{"+join(degrees_list,",")+"}"
        return degrees_string

    def compute_irreducible_components_to_view(self,partition):
        m2 = self.compute_irreducible_components(partition)
        num_components = int(m2.eval("length M"))
        output = []
        output.append("Partition = %s :" % str(partition))
        for i in xrange(num_components):
            output.append(m2.eval("M_%d" % i ))
        return output




def compute_orbital_varieties_up_to_N(N,out_file=None,mac2=None):
    if mac2 is None:
        mac2 = Macaulay2()
    calc = {}
    for n in range(2,N+1):
        calc[n] = SL_n_Orbital_Variety_Calculator(n)
        for partition in Partitions(n):
            if out_file is not None:
                mac2 = calc[n].compute_irreducible_components_with_gradings_pass_mac2_append_to_file(
                    partition,mac2,out_file)
            else:
                mac2 = calc[n].compute_irreducible_components_with_gradings_pass_mac2(
                    partition,mac2)
                
    return mac2



def compute_tableau_of_irreducible_components_up_to(N,mac2):

    pre_tableaux_dict = {}
    for n in range(2,N+1):
        if n == 2:
            pre_tableaux_dict["M11#0"] = Partition([1])
            pre_tableaux_dict["M2#0"] = Partition([1])
        else:
            for partition1 in Partitions(n):
                num_components1 = mac2("length M"+partition_as_number_string(partition1)).to_sage()
                for i in xrange(num_components1):
                    found = False
                    mac2string_first_part = ("substitute(M" +
                                              partition_as_number_string(partition1)+"#"
                                              +str(i)+ ",R"+str(n-1)+")")

                    if found:
                        continue
                    for partition2 in Partitions(n-1):
                        num_components2 = mac2("length M"+partition_as_number_string(partition2)).to_sage()
                        for j in xrange(num_components2):
                            mac2string_second_part = ("substitute(M" +
                                                      partition_as_number_string(partition2)+"#"
                                                      +str(j)+ ",R"+str(n-1)+")")
                            # print mac2string_first_part
                            # print mac2string_second_part
                            # print mac2("ring " + mac2string_second_part)
                            # print mac2("R2")
                            if mac2(mac2string_first_part+"=="+mac2string_second_part).to_sage() is True:
                                key = "M"+partition_as_number_string(partition1)+"#"+str(i)
                                value = "M"+partition_as_number_string(partition2)+"#"+str(j)
                                pre_tableaux_dict[key] = value
                                found = True
                                break
    return mac2,pre_tableaux_dict

def partition_chain_dict_from_pre_tableaux_dict(N,mac2,pre_tableaux_dict):
    for n in xrange(2,6):
        partition_chain_dict = {}
        for n in range(2,N+1):
            if n == 2:
                partition_chain_dict["M11#0"] = [Partition([]),Partition([1]),Partition([1,1])]
                partition_chain_dict["M2#0"] = [Partition([]),Partition([1]),Partition([2])]
            else:
                for partition in Partitions(n):
                    num_components = mac2("length M"+partition_as_number_string(partition)).to_sage()
                    for i in xrange(num_components):
                        key = "M"+partition_as_number_string(partition)+"#"+str(i)
                        value = pre_tableaux_dict[key]
                        partition_chain_dict[key] = partition_chain_dict[value]+[partition]
    tableaux_dict = {key : from_chain(partition_chain_dict[key]) for key in partition_chain_dict.keys()}
    return mac2,tableaux_dict

