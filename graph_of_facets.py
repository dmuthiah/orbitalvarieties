from distinguished_facets import skew_tableau_from_list_of_positions_complement

#Uncomment below to compute facets
#load('matrix_of_springer_representation.py')
#positions_dict = compute_positions_up_to_7()


def graph_of_facets(tableau,positions_dict):
    """ 
    This is essentially Alejandro's function for computing 
    the graph of mutations.
    """
    my_list = positions_dict[tableau]
    n = tableau.size()
    f = lambda x : skew_tableau_from_list_of_positions_complement(x,n)
    vert = [tuple(d) for d in my_list]
    edges = []
    PP = combinations(vert,2)
    for p in PP:
        S1 = set(p[0])
        S2 = set(p[1])
        if len(S1.symmetric_difference(S2)) == 2:
            edges.append(p)
    vert_as_facets = map(f,vert)
    edges_as_facets = [ map(f,edge) for edge in edges]
    return Graph([vert_as_facets,edges_as_facets])
